import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router'

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.page.html',
  styleUrls: ['./candidate-list.page.scss'],
})



export class CandidateListPage implements OnInit {
  list = [];
  constructor(public alertController: AlertController, private router: Router) {}



ngOnInit() {
  this.list = [
    { id: 1, name: 'Cyril Ramaphosa', image: '../../assets/cyril.jpg', party: 'ANC' },
    { id: 2, name: 'Julius Malema', image: '../../assets/malema.jpg',  party: 'EFF'},
    { id: 3, name: 'John Steenhuisen', image: '../../assets/john.jpg',  party: 'DA' }
];
  }

  async vote(name: string): Promise<void>{
    const alert = await this.alertController.create({
      header: 'Warning',
      message: 'Are you sure?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        }, {
          text: 'Yes',
          handler: async () => {
            const alert2 = await this.alertController.create({
              message: 'You have voted for ' + name,
              buttons: [   {
                text: 'Yes',
                handler: () => {
                  this.router.navigateByUrl('/login');
                }
              }



              ]
            });
           
            await alert2.present();

          }
        },

      ]
    });

    await alert.present();
  }

}
