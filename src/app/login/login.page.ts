import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  id: string;
  name: string;
  error: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  submit(): void{
    if (!this.id || !this.name){
      this.error = 'Please fill your ID and name';
    }else{
      this.error = null;
      this.router.navigateByUrl('candidate');
    }

  }

}
